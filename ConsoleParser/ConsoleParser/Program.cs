﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace ConsoleParser
{
    class Program
    {
        static void Main(string[] args)
        {

            string test1 = "this.is.a.test";
            string test2 = "another, test! really.";
            string test3 = "split around || double pipes";

            //using a single char
            string[] tokens1 = test1.Split('.');

            //using many chars
            string[] tokens2 = test2.Split(',', ' ', '!');

            //using many chars with StringSplitOptions
            string[] tokens3 = test2.Split(new[] { ',', ' ', '!' }, StringSplitOptions.None);

            //using strings and StringSplitOptions
            //note, there is no method that splits around a single string
            //also, all overloads that use strings as splitters require a StringSplitOption
            string[] tokens4 = test3.Split(new[] { "||" }, StringSplitOptions.RemoveEmptyEntries);


            Console.WriteLine("Program started");

            var path = "http://btctrader-news.azurewebsites.net/2014/12/19/haber-linkleri/";

            var html = "";

            var result = "";
            using (WebClient wc = new WebClient())
            {
                html = wc.DownloadString(path);
            }


            result = html.Substring(100);

            //basit yol
            //http://www.dreamincode.net/forums/topic/329281-string-manipulation/

            //Gelişmiş
            //http://htmlagilitypack.codeplex.com/


            Console.WriteLine(html);
            Console.WriteLine("Program ended");
            Console.Read();
        }
    }
}
